To execute Rest Services. 

1: Download latest version of Eclipse IDE for J2EE development
pull the code to your derectory using 

Initialize empty repository in your folder: git init
Add remote origin:  git remote add origin https://gitlab.com/shripalshah74/FastSpringAssignment.git
command: git fetch --all
Pull comand: git pull origin master.



>>After you have configured gitlab, 

>>Open Eclipse, do the foll steps:

>>File -> Import -> (Search ) Existing Maven Projects

>>Go to the directory where you have pulled code from the gitlab.
select the folder "restful-web-services"

Now, in the project explorer -> navigate the foll to execute the code.

>> restful-web-service
	>>src/main/java
		>>com.fastspring.codingassignment.restfulwebservices (Package)
			RestfulWebServicesApplication.java
				|__\
				   /  Right click this file and run as Java app.
Use the postman COllection to test

	

Things  Cover in this app assignment:
-> Basic REST SERVICE to fullfill functionality
->Have implemented Static database 

Partially covered

Due to busy schedule, I couldn't finish the UI part. 
You can find my-pizza-app folder in the git repository. I have used Angular for my UI.
To execute the partial frontend app, 
>> DOwnload Node.js from https://nodejs.org/en/download/
>> Download Angular CLI using sudo npm install -g @angular/cli
	>> GO to the app directory and un the following command:
		ng serve
		navigate to localhost:4200 to have a look at partially developed UI.


I have also attached 2 snapshots of the UI in the REPO.


I have following Entities in my backend packaged in different packages:
LoginManager.java 
	|_ To Map Managers login credentials

Manager.java
	|_ To Map Managers details


LoginUser.java
	|_ To Map User login credentials
User.java
	|_ To map to Users details

Orders.java
	|_ To Map to Orders details

Pizzas.java
	|_ To Map to Pizza details

Toppings.java
	|_ To Map to Topping details 


Each Package has its own following files:
|_ EntityDao.java
|_ EntityResource.java
|_ EntitySpecificExceptions.java for Company specific exception structure.


 