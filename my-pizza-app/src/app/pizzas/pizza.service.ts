import {Pizza} from './pizza-list/pizza.model';
import {EventEmitter} from '@angular/core';

export class PizzaService {

  pizzaSelected = new EventEmitter<Pizza>()
  private pizzas: Pizza[] = [
    new Pizza(1, 'Cheese', 10, 'http://halalfoodexpress.com/wp-content/uploads/2016/12/Cheese-Pizza-1.jpg'),
    new Pizza(2, 'Veggie', 15, 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/img-1173-1516914093.jpeg')
  ];

  getPizzas() {
    return this.pizzas.slice();
  }
}

