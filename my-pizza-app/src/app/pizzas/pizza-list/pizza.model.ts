export class Pizza {
  public id: number;
  public name: string;
  public cost: number;
  public imagePath: string


  constructor(id: number, name: string, cost: number, imagePath: string) {
    this.id = id;
    this.name = name;
    this.cost = cost;
    this.imagePath = imagePath;
  }
}
