import {Component, Input, OnInit} from '@angular/core';
import {Pizza} from '../pizza.model';
import {PizzaService} from '../../pizza.service';

@Component({
  selector: 'app-pizza-item',
  templateUrl: './pizza-item.component.html',
  styleUrls: ['./pizza-item.component.css']
})
export class PizzaItemComponent implements OnInit {
@Input()  pizza: Pizza;
  constructor(private pizzaService: PizzaService) { }

  ngOnInit() {
  }
  onSelected() {
    this.pizzaService.pizzaSelected.emit(this.pizza);
  }
}
