import { Component, OnInit } from '@angular/core';
import {Pizza} from './pizza-list/pizza.model';
import {PizzaService} from './pizza.service';

@Component({
  selector: 'app-pizzas',
  templateUrl: './pizzas.component.html',
  styleUrls: ['./pizzas.component.css'],
  providers: [PizzaService]
})
export class PizzasComponent implements OnInit {
  selectedPizza: Pizza;
  constructor(private pizzaService: PizzaService) { }

  ngOnInit() {
    this.pizzaService.pizzaSelected.subscribe((pizza: Pizza) => {
      this.selectedPizza = pizza;
    });
  }

}
