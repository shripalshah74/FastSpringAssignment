package com.fastspring.codingassignment.restfulwebservices.order;

import java.util.Date;

public class Order {

	private Integer id;
	private Integer uId;
	private Integer pId;
	private Integer [] tIds;
	private Integer cost;
	private Date dateOfOrder;
	
	public Order() {
		
	}
	public Order(Integer id, Integer uId, Integer pId, Integer[] tIds, Integer cost) {
		super();
		this.id = id;
		
		this.uId = uId;
		this.pId = pId;
		this.tIds = tIds;
		this.cost = cost;
		this.dateOfOrder = new Date();
		
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getuId() {
		return uId;
	}
	public void setuId(Integer uId) {
		this.uId = uId;
	}
	public Integer getpId() {
		return pId;
	}
	public void setpId(Integer pId) {
		this.pId = pId;
	}
	public Integer[] gettIds() {
		return tIds;
	}
	public void settIds(Integer[] tIds) {
		this.tIds = tIds;
	}
	
	public Integer getCost() {
		return cost;
	}
	public void setCost(Integer cost) {
		this.cost = cost;
	}
	@Override
	public String toString() {
		return String.format("Orders[id=%s, UserId=%s, PizzaId=%s, ToppingId=%s, Cost=%s ]", id, uId, pId, tIds, cost );
	}
	
	
}
