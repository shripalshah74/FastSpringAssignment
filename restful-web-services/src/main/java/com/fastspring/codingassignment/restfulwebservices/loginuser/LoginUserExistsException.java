package com.fastspring.codingassignment.restfulwebservices.loginuser;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class LoginUserExistsException extends RuntimeException {

	public LoginUserExistsException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
	

}
