package com.fastspring.codingassignment.restfulwebservices.pizza;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class PizzaDaoService {
	private static List <Pizza> pizzas = new ArrayList<>();
	private static int pizzaCount = 2;
	static {
		pizzas.add(new Pizza(1,"Cheese Pizza", 12));
		pizzas.add(new Pizza(2,"Veggie Pizza", 10));
	}
	
	// Get the List of Pizza's
	public List <Pizza> findAll() {
		return pizzas;
	}
	
	
	// Add a new Pizza in the Menue
	public Pizza save(Pizza pizza) {
		if(pizza.getId()==null) {
			pizza.setId(++pizzaCount);
		}
		pizzas.add(pizza);
		return pizza;
	}
	
	// find a specific Pizza by Id
	
	public Pizza findOne(int id) {
		for(Pizza pizza: pizzas) {
			if(pizza.getId() == id) {
				return pizza;
			}
		}
		
		return null;
		
	}

	// delete a Pizza by id
	
	public Pizza deleteById(int id) {
		Iterator <Pizza> iterator = pizzas.iterator();
		while(iterator.hasNext()) {
			Pizza pizza = iterator.next();
			if(pizza.getId() == id) {
				iterator.remove();
				return pizza;
			}
		}
		return null;
	}
	
	
	
	
}
