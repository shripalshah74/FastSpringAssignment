package com.fastspring.codingassignment.restfulwebservices.loginmanager;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class LoginManagerExistsException extends RuntimeException {

	public LoginManagerExistsException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
	
}
