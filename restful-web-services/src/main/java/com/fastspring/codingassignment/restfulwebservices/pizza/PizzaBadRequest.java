package com.fastspring.codingassignment.restfulwebservices.pizza;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class PizzaBadRequest extends RuntimeException {

	public PizzaBadRequest(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}
	
		
}
