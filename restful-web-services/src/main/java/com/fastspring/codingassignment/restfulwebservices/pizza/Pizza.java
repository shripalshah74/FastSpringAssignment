package com.fastspring.codingassignment.restfulwebservices.pizza;

public class Pizza {
	
	private Integer id;
	private String name;
	private Integer basePrice;
	
	
	protected Pizza () {
		
	}
	
	
	public Pizza(Integer id, String name, Integer basePrice) {
		super();
		this.id = id;
		this.name = name;
		this.basePrice = basePrice;
	}
	
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getBasePrice() {
		return basePrice;
	}
	public void setBasePrice(Integer basePrice) {
		this.basePrice = basePrice;
	}
	
	
	@Override 
	
	public String toString() {
		return String.format("Pizza [id=%s, name=%s, basePrice=%s]", id, name, basePrice);
	}
	
	

}
