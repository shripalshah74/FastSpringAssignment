package com.fastspring.codingassignment.restfulwebservices.manager;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fastspring.codingassignment.restfulwebservices.user.User;
import com.fastspring.codingassignment.restfulwebservices.user.UserBadRequest;

@RestController
public class ManagerResource {
	@Autowired
	private ManagerDaoService service;
	
	// We may have Super Manager Class for Operators Audience(Branch Owners)
//	@GetManagers("/managers")
	
	
	@GetMapping("/managers/{id}")
	public Manager retrieveManager(@PathVariable int id) {
		Manager manager = service.findOne(id);
		if(manager == null) {
			throw new ManagerNotFoundException("id-"+ id);
		}
		return manager;
	}
	
	
}
