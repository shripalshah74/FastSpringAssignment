package com.fastspring.codingassignment.restfulwebservices.loginmanager;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fastspring.codingassignment.restfulwebservices.manager.ManagerNotFoundException;

@RestController
public class LoginManagerResource {

	@Autowired
	private LoginManagerDaoService service;
	
	@PostMapping("/managers/signup")
	public ResponseEntity<Object> createManager(@Valid @RequestHeader String name,@Valid @RequestBody LoginManager manager) {
		// check whether the manager exists
		String email = manager.getEmail();
		if(service.findOne(email) != null) {
			
			throw new LoginManagerExistsException("email-"+ email+" exists! Please signIn");
			
		}		
		LoginManager savedLoginManager = service.save(manager, name );
		
		// Status of CREATED
		// /managers/{id}      <-savedManager.getId
		URI location = ServletUriComponentsBuilder
		.fromCurrentRequest()
		.path("/{id}")
		.buildAndExpand(savedLoginManager.getId()).toUri();
		return ResponseEntity.created(location).build();
		
		
	}
	
	@PostMapping ("/managers/signin")
	public int signInManager (@Valid @RequestBody LoginManager manager ) {
		String email = manager.getEmail();
		String emailFromDatabase = service.findOne(email).getEmail();
		if(emailFromDatabase == null) {
			throw new ManagerNotFoundException("Manager -"+ email + " Not Found! Please Signup!");
		}
		else if(email.equals(emailFromDatabase)) {
			if(manager.getPassword().equals(service.findOne(email).getPassword())) {
				return service.findOne(email).getId(); 
			}
			else {
				throw new ManagerCredentialsAreWrong("Wrong Credentials");
			}
		}
		else {
			throw new ManagerCredentialsAreWrong("Wrong Credentials");
		}
		
	}
	
	
	
	
	
}
