package com.fastspring.codingassignment.restfulwebservices.topping;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
public class ToppingResource {

	@Autowired
	private ToppingDaoService service;
	
	// GET /toppings
	// retrieveAllToppings
	@GetMapping("/toppings")
	public List<Topping> retrieveAllToppings() {
		return service.findAll();
	}
	
	//GET /toppings/{id}
	// retrieveTopping(int id)
	
	@GetMapping("/toppings/{id}")
	public Topping retrieveTopping(@PathVariable int id) {
		Topping topping = service.findOne(id);
		if(topping == null) {
			throw new ToppingNotFoundException("id-"+ id);
		}
		return topping;
	}
	
	// CREATED 
	// input - details of toppings
	// output - Created & Return the Created URI
	
	@PostMapping("/toppings")
	public ResponseEntity<Object> createTopping(@RequestBody Topping topping) {
		
		if(topping.getCost() == null|| topping.getName() == null || topping == null) {
			throw new ToppingBadRequest("topping-"+ topping + "name and cost are required");
		}
		
		Topping savedTopping = service.save(topping);
		
		// Status of CREATED
		// /topping/{id) <- savedTopping.getId
		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(savedTopping.getId()).toUri();
		return ResponseEntity.created(location).build();
		
	}
	
	
	@DeleteMapping("/toppings/{id}")
	public void deleteTopping(@PathVariable int id) {
		Topping topping = service.deleteById(id);
		if(topping == null) {
			throw new ToppingNotFoundException("id-"+ id);
		}
	}
	
	
	
}
