package com.fastspring.codingassignment.restfulwebservices.topping;

public class Topping {
	
	private Integer id;
	private String name;
	private Integer cost;
	
	protected  Topping() {
		
	}
	
	public Topping(Integer id, String name, Integer cost) {
		super();
		this.id = id;
		this.name = name;
		this.cost = cost;
	}
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getCost() {
		return cost;
	}
	public void setCost(Integer cost) {
		this.cost = cost;
	}
	
	
	@Override
	
	public String toString() {
		return String.format("Topping [id=%s, name=%s, cost=%s]", id, name, cost);
	}
	
	
	
	
}
