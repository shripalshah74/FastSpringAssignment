package com.fastspring.codingassignment.restfulwebservices.user;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UserBadRequest extends RuntimeException {

	public UserBadRequest(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}
	
	
}
