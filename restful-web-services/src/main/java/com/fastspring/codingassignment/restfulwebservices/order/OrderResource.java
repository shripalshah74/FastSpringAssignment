package com.fastspring.codingassignment.restfulwebservices.order;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.fastspring.codingassignment.restfulwebservices.manager.ManagerDaoService;
import com.fastspring.codingassignment.restfulwebservices.manager.ManagerNotFoundException;

@RestController
public class OrderResource {
	@Autowired
	private OrderDaoService service;

	@Autowired
	private ManagerDaoService managerService;
	
	// GET /orders
	// retrieveAllOrders
	// for manager
	@GetMapping("/orders")
	public List<Order> retrieveAllOrders(@RequestHeader int mid ){
		if(managerService.findOne(mid) == null) {
			throw new ManagerNotFoundException("Manager-"+ mid +" Not Found");
		}
		return service.findAll();
	}
	
	
	// for managers
	@GetMapping("/orders/{id}")
	public Order retrieveOrder (@RequestHeader int mid, @PathVariable int id ) {
		//TO DO // check for valid manager ID
		if(managerService.findOne(mid) == null) {
			throw new ManagerNotFoundException("Manager-"+ mid +" Not Found");
		}
		Order order = service.findOne(id);
		if(order == null) {
			throw new OrderNotFoundException("id-"+ id);
		}
		
		return order;
		
	}
	
	
	
	
}
