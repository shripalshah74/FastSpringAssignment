package com.fastspring.codingassignment.restfulwebservices.topping;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus (HttpStatus.NOT_FOUND)
public class ToppingNotFoundException extends RuntimeException {

	public ToppingNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	
}
