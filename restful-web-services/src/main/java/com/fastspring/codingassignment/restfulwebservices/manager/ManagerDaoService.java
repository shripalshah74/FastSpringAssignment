package com.fastspring.codingassignment.restfulwebservices.manager;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class ManagerDaoService {
	
	private static List<Manager> managers = new ArrayList<>();
	private static int managersCount = 4;
	static {
		managers.add(new Manager(1,"Shripal Shah"));		
	}
	
	
	public Manager save( int id, String name) {
		Manager manager = new Manager (id,name);
		managers.add(manager);
		return manager;
	}
	
	public Manager findOne(int id) {
		for(Manager manager: managers) {
			if(manager.getId() == id) {
				return manager;
			}
		}
		
		return null;
	}
	
	
	

}
