package com.fastspring.codingassignment.restfulwebservices.loginmanager;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class ManagerCredentialsAreWrong extends RuntimeException {

	public ManagerCredentialsAreWrong(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
	
}
