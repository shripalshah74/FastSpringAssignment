package com.fastspring.codingassignment.restfulwebservices.pizza;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
public class PizzaResource {

	@Autowired
	private PizzaDaoService service;
	
	// GET /pizzas
	// retrieveAllPizzas
	
	@GetMapping("/pizzas")
	public List<Pizza> retrieveAllPizza(){
		return service.findAll();
	}
	
	
	//GET /pizzas/{id}
	
	@GetMapping("/pizzas/{id}")
	public Pizza retrievePizza(@PathVariable int id) {
		
		Pizza pizza =  service.findOne(id);
		if(pizza == null) {
			throw new PizzaNotFoundException("id-"+ id);
		}
		return pizza;
	}
	
	
	@PostMapping("/pizzas")
	public ResponseEntity<Object> createPizza(@RequestBody Pizza pizza) {
		
		if(pizza.getName()==null || pizza.getBasePrice()==null) {
			throw new PizzaBadRequest("pizza-"+ pizza +"name and basePrice both are required ");
		}
		
		Pizza savedPizza = service.save(pizza);
		
		// Status CREATED
		// /pizzas/{id}  <- savePizza.getId
		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(savedPizza.getId()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	
	
	@DeleteMapping("/pizzas/{id}")
	public void deletePizza(@PathVariable int id) {
		Pizza pizza = service.deleteById(id);
		if(pizza == null) {
			throw new PizzaNotFoundException("id-"+ id);
		}
	}
	
	
}
