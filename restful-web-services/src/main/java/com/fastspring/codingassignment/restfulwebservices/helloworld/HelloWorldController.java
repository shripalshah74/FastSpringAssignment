package com.fastspring.codingassignment.restfulwebservices.helloworld;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

//Tell Spring that this is the Controller

@RestController
public class HelloWorldController {

	//GET
	//URI - /hello-world
	// method - "HEllo World
	@GetMapping(path="/hello-world")
	public String helloWorld () {
		return "Hello World";
	}
	
	
	// hello-world-bean
	@GetMapping(path="/hello-world-bean")
	public HelloWorldBean helloWorldBean () {
		return new HelloWorldBean("Hello World");
	}
	
	
	// hello-world/path-variable/fastspring
		@GetMapping(path="/hello-world/path-variable/{name}")
		public HelloWorldBean helloWorldPathVariable (@PathVariable String name) {
			return new HelloWorldBean(String.format("Hello World, %s", name));
		}
	
}
