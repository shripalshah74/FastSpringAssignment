package com.fastspring.codingassignment.restfulwebservices.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class UserDaoService {
	
	private static List<User> users = new ArrayList<>();
	
	static {
		users.add(new User(1, "Dave"));
		
		
	}
	
	public List <User> findAll() {
		return users;
	}
	
	public User save(int id, String name) {
			User user = new User (id,name);
			users.add(user);
			return user;
	}
	public User findOne(int id) { 
		for(User user: users) {
			if(user.getId() == id) {
				return user;
			}
			
			
		}
		return null;
	} 
	
	
	
	public User deleteById(int id) {
		Iterator <User> iterator = users.iterator(); 
		while(iterator.hasNext()) {
			User user = iterator.next();
			if(user.getId() == id) {
				iterator.remove();// code to delete
				return user;
			}
			
			
		}
		return null;
	} 
	
}
