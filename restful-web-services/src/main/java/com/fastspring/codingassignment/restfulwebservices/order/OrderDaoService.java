package com.fastspring.codingassignment.restfulwebservices.order;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.fastspring.codingassignment.restfulwebservices.pizza.Pizza;
import com.fastspring.codingassignment.restfulwebservices.pizza.PizzaDaoService;
import com.fastspring.codingassignment.restfulwebservices.topping.Topping;
import com.fastspring.codingassignment.restfulwebservices.topping.ToppingDaoService;
import com.fastspring.codingassignment.restfulwebservices.user.UserDaoService;

@Component
public class OrderDaoService {
	@Autowired
	private PizzaDaoService servicePizza;
	
	@Autowired
	private ToppingDaoService serviceTopping;
	
	@Autowired
	private UserDaoService serviceUser;
	
	private static List<Order> orders = new ArrayList<>();
	private static int orderCount = 2;
	
	static {
		orders.add(new Order(1,1,1,new Integer[] {1,2},20));
		orders.add(new Order(2,1,1,new Integer[] {1,2,3},20));
	}
	
	// for Managers
	public List <Order> findAll() {
		
		return orders;
	}
	
	
	public Order findOne(int uid) {
		for(Order order : orders) {
			if(order.getuId()== uid) {
				return order;
			}
		}
		return null;
	}

	// for Users as well as Managers
	
	public List<Order> findAllUserOrders(int uid) {
		List<Order> userOrder = new ArrayList<>();
		for(Order order : orders) {
			if(order.getuId() == uid) {
				userOrder.add(order);
			}
		}
		return userOrder;
	}
	
	
	// Saving the Order 
	
	public Order save(Order order) {
		int cost = 0;
		
		if(serviceUser.findOne(order.getuId()) != null) {
			cost= cost + servicePizza.findOne(order.getpId()).getBasePrice();
			for(int tId : order.gettIds()) {
				cost = cost + serviceTopping.findOne(tId).getCost();
			}
			
			order.setCost(cost);
			
			
			if(order.getId() == null) {
				order.setId(++orderCount);
			}
			orders.add(order);
			return order;
		
		}
		
		return null;
		
	}
	
}
 