package com.fastspring.codingassignment.restfulwebservices.topping;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ToppingBadRequest extends RuntimeException {

	public ToppingBadRequest(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}
	
}
