package com.fastspring.codingassignment.restfulwebservices.loginuser;

import java.net.URI;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fastspring.codingassignment.restfulwebservices.loginmanager.LoginManager;
import com.fastspring.codingassignment.restfulwebservices.loginmanager.ManagerCredentialsAreWrong;
import com.fastspring.codingassignment.restfulwebservices.manager.ManagerNotFoundException;

@RestController
public class LoginUserResource {
	
	@Autowired
	private LoginUserDaoService service;
//	
	@PostMapping("/users/signup")
	public ResponseEntity<Object> createUser(
			@Valid @RequestHeader String name, 
			@Valid @RequestBody LoginUser user ){
		
		// check whether the user exists 
		String email = user.getEmail();
		if(service.findOne(email)!=null) {
			throw new LoginUserExistsException("email-"+ email+ "exists! Please signIn");
		}
		
		LoginUser savedLoginUser = service.save(user, name);
		
		// Status of CREATED
		// /users/{id}    <-savedManager.getId
		
		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(savedLoginUser.getId()).toUri();
		
		return ResponseEntity.created(location).build();
	}
	
	
	@PostMapping ("users/signin")
	public int signInUser (@Valid @RequestBody LoginUser user ) {
		String email = user.getEmail();
		String emailFromDatabase = service.findOne(email).getEmail();
		if(emailFromDatabase == null) {
			throw new ManagerNotFoundException("User -"+ email + " Not Found! Please Signup!");
		}
		else if(email.equals(emailFromDatabase)) {
			if(user.getPassword().equals(service.findOne(email).getPassword())) {
				return service.findOne(email).getId(); 
			}
			else {
				throw new ManagerCredentialsAreWrong("Wrong Credentials");
			}
		}
		else {
			throw new ManagerCredentialsAreWrong("Wrong Credentials");
		}
		
	}
	
	

}
