package com.fastspring.codingassignment.restfulwebservices.manager;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Manager {
	
	private Integer id;
	
	@NotEmpty
	@NotNull
	@Size(min=2)
	private String name;
	
	protected Manager() {
		
	}
	
	
	
	public Manager(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}



	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	

}
 