package com.fastspring.codingassignment.restfulwebservices.topping;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class ToppingDaoService {
	
	private static List <Topping> toppings = new ArrayList<>();
	private static int toppingCount = 3;
	
	static {
		toppings.add(new Topping(1, "Tomatoes" , 1));
		toppings.add(new Topping(2, "Olives" , 1));
		toppings.add(new Topping(3, "Onions" , 1));
		toppings.add(new Topping(4, "Pineapples" , 1));		
	}
	
	// Get the List of available Toppings
	
	public List <Topping> findAll(){
		return toppings;
	}
	
	
	// Add a new Topping in the Menu
	
	public Topping save(Topping topping) {
		if(topping.getId()==null) {
			topping.setId(++toppingCount);
		}
		toppings.add(topping);
		return topping;
	}
	
	// find a specific Topping by Id
	
	public Topping findOne (int id) {
		for(Topping topping : toppings) {
			if(topping.getId() == id) {
				return topping;
			}
		}
		
		return null;
	}
	
	// delete the Topping by Id 
	
	public Topping deleteById (int id) {
		
		Iterator <Topping> iterator = toppings.iterator();
		
		while (iterator.hasNext()) {
			
			Topping topping = iterator.next();
			if(topping.getId() == id) {
				iterator.remove();// code to delete
				return topping;
			}
		}
		
		return null;
	}
	
	
}
