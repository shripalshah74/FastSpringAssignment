package com.fastspring.codingassignment.restfulwebservices.user;

import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fastspring.codingassignment.restfulwebservices.order.Order;
import com.fastspring.codingassignment.restfulwebservices.order.OrderDaoService;
import com.fastspring.codingassignment.restfulwebservices.pizza.PizzaDaoService;
import com.fastspring.codingassignment.restfulwebservices.pizza.PizzaNotFoundException;
import com.fastspring.codingassignment.restfulwebservices.topping.Topping;
import com.fastspring.codingassignment.restfulwebservices.topping.ToppingDaoService;
import com.fastspring.codingassignment.restfulwebservices.topping.ToppingNotFoundException;

import javax.validation.Valid;

@RestController
public class UserResource {

	@Autowired
	private UserDaoService service;
	
	@Autowired
	private OrderDaoService orderService;
	
	@Autowired
	private PizzaDaoService pizzaService;
	
	@Autowired
	private ToppingDaoService toppingService;
	
	// GET /users
	//retrieveAllUsers
	@GetMapping("/users")
	public List<User> retrieveAllUsers() {
		return service.findAll();
	}
	
	
	//GET /users/{id}
	//retrieveUser(int id)
	
	@GetMapping("/users/{id}")
	public User retrieveUser(@PathVariable int id) {
		User user = service.findOne(id);
		if(user == null) {
			throw new UserNotFoundException("id-"+ id);
		}
		return user;
	}
	
	 
	// CREATED
	// input - details of user
	// output - CREATED & Return the created URI
	
	/*@PostMapping("/users")
	public ResponseEntity<Object> createUser(@Valid @RequestBody User user) {
		
		if(user.getBirthDate() == null) {
			throw new UserBadRequest("user-"+ user + "dateOfBirth Should Not be null" );
		}
		User savedUser = service.save(user);
		
		System.out.println(user);
		// Status of CREATED
		// /user/{id}      <-savedUser.getId
		URI location = ServletUriComponentsBuilder
		.fromCurrentRequest()
		.path("/{id}")
		.buildAndExpand(savedUser.getId()).toUri();
		return ResponseEntity.created(location).build();
		
		
	}*/
	
	
	
	@DeleteMapping("/users/{id}")
	public void deleteUser(@PathVariable int id) {
		
		User user = service.deleteById(id);
		if(user == null) {
			throw new UserNotFoundException("id-"+ id);	
		}
		
	}
	
	// GET orders pertaining to specific user
	@GetMapping("/users/orders")
	public List<Order> retrieveOrders(@RequestHeader int uid){
		User user = service.findOne(uid);
		if(user == null) {
			throw new UserNotFoundException("id-"+ uid);
		}
		return orderService.findAllUserOrders(user.getId());
	}
	
	// POST Order for a specific User
	
	@PostMapping("/users/orders")
	public ResponseEntity<Object> createOrder(@RequestHeader int uid ,@RequestBody Order order) {
		
		if(service.findOne(uid) == null) {
			throw new UserNotFoundException("id-"+ uid);
		}
		
		order.setuId(uid);
		
		if(order.getpId() == null) {
			throw new UserBadRequest("Order-"+ order + "need to add a pizza!" );
		}
		if(pizzaService.findOne(order.getpId()) == null){
			throw new PizzaNotFoundException("Pizza not found!" );
		}
		
		for(int tIds : order.gettIds()) {
			if(toppingService.findOne(tIds) == null) {
				throw new ToppingNotFoundException("Topping id- "+ tIds +"Not Found");
			}
		}
		
		Order savedOrder = orderService.save(order);
		// Status of CREATED
		// /user/orders/{id}      <-savedOrderer.getId
		URI location = ServletUriComponentsBuilder
		.fromCurrentRequest()
		.path("/{id}")
		.buildAndExpand(savedOrder.getId()).toUri();
		return ResponseEntity.created(location).build();
		
		
	}
	
	
	
	
	
}
