package com.fastspring.codingassignment.restfulwebservices.exception;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fastspring.codingassignment.restfulwebservices.pizza.PizzaBadRequest;
import com.fastspring.codingassignment.restfulwebservices.pizza.PizzaNotFoundException;
import com.fastspring.codingassignment.restfulwebservices.topping.ToppingBadRequest;
import com.fastspring.codingassignment.restfulwebservices.topping.ToppingNotFoundException;
import com.fastspring.codingassignment.restfulwebservices.user.UserBadRequest;
import com.fastspring.codingassignment.restfulwebservices.user.UserNotFoundException;

@ControllerAdvice

@RestController
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
	
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> handleAllException(Exception ex, WebRequest request) {
		
		ExceptionResponse exceptionResponse =  new ExceptionResponse(new Date(), ex.getMessage(),request.getDescription(false));
		return new ResponseEntity(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	
	
	@ExceptionHandler(UserNotFoundException.class)
	public final ResponseEntity<Object> handleUserNotFoundException(UserNotFoundException ex, WebRequest request) {
		
		ExceptionResponse exceptionResponse =  new ExceptionResponse(new Date(), ex.getMessage(),request.getDescription(false));
		return new ResponseEntity(exceptionResponse, HttpStatus.NOT_FOUND);
	}
	
	
	@ExceptionHandler(PizzaNotFoundException.class)
	public final ResponseEntity<Object> handlePizzaNotFoundException(PizzaNotFoundException ex, WebRequest request) {
		
		ExceptionResponse exceptionResponse =  new ExceptionResponse(new Date(), ex.getMessage(),request.getDescription(false));
		return new ResponseEntity(exceptionResponse, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(ToppingNotFoundException.class)
	public final ResponseEntity<Object> handleToppingNotFoundException(ToppingNotFoundException ex, WebRequest request) {
		
		ExceptionResponse exceptionResponse =  new ExceptionResponse(new Date(), ex.getMessage(),request.getDescription(false));
		return new ResponseEntity(exceptionResponse, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(UserBadRequest.class)
	public final ResponseEntity<Object> handleUserBadRequestException(UserBadRequest ex, WebRequest request) {
		
		ExceptionResponse exceptionResponse =  new ExceptionResponse(new Date(), ex.getMessage(),request.getDescription(false));
		return new ResponseEntity(exceptionResponse, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(ToppingBadRequest.class)
	public final ResponseEntity<Object> handleToppingBadRequestException(ToppingBadRequest ex, WebRequest request) {
		
		ExceptionResponse exceptionResponse =  new ExceptionResponse(new Date(), ex.getMessage(),request.getDescription(false));
		return new ResponseEntity(exceptionResponse, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(PizzaBadRequest.class)
	public final ResponseEntity<Object> handlePizzaBadRequestException(PizzaBadRequest ex, WebRequest request) {
		
		ExceptionResponse exceptionResponse =  new ExceptionResponse(new Date(), ex.getMessage(),request.getDescription(false));
		return new ResponseEntity(exceptionResponse, HttpStatus.BAD_REQUEST);
	}
	
	

}
