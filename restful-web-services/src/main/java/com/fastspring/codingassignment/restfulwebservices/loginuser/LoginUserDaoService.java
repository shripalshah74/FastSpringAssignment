package com.fastspring.codingassignment.restfulwebservices.loginuser;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fastspring.codingassignment.restfulwebservices.user.UserDaoService;

@Component
public class LoginUserDaoService {
	
	@Autowired
	private UserDaoService userService;
	private static List<LoginUser> users = new ArrayList<>();
	private static int userCount = 1;
	
	static {
		users.add(new LoginUser(1, "dave@gmail.com", "Pass@123"));
	}
	
	public LoginUser save(LoginUser user, String name) {
		
		String userEmail = user.getEmail();
		user.setId(++userCount);
		if(userService.save(userCount, name) != null) {
			users.add(user);
		}
		
		return user;
		
	}
	
	public LoginUser findOne(String email) {
		for(LoginUser loginUser : users) {
			if(loginUser.getEmail().equals(email)) {
				return loginUser;
			}
		}
		return null;
	}
	
	
	
	
}
