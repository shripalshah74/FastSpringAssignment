package com.fastspring.codingassignment.restfulwebservices.loginuser;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
//@Entity
public class LoginUser {
//	@Id
//	@GeneratedValue
	private Integer id;
	
	@NotNull
	@Email
	@NotEmpty
	private String email;
	
	
	@Size(min=5)
	private String password;


	public LoginUser(Integer id, @NotNull @Email @NotEmpty String email, @Size(min = 5) String password) {
		super();
		this.id = id;
		this.email = email;
		this.password = password;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}

	
	
}
