package com.fastspring.codingassignment.restfulwebservices.loginmanager;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fastspring.codingassignment.restfulwebservices.manager.ManagerDaoService;
@Component
public class LoginManagerDaoService {

	@Autowired
	private ManagerDaoService managerService;
	private static List<LoginManager> managers = new ArrayList<>();
	private static int managerCount = 1;
	
	static {
		managers.add(new LoginManager(1, "shripalshah74@hotmail.com", "Pass@123" ));
	}
	
	
	public LoginManager save(LoginManager manager, String name) {
		String managerEmail = manager.getEmail();
		manager.setId(++managerCount);
		if(managerService.save(managerCount, name )!= null) {
			managers.add(manager);
		}
		return manager;
	}
	
	public LoginManager findOne(String email) {
		for (LoginManager loginManager : managers) {
			System.out.println("ForLoop inside findone");
			if(loginManager.getEmail().equals(email)) {
				return loginManager;
			}
			
		}
		
		return null;
	}
	
}
